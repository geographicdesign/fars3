import os
import codecs


CSV_DIR = "../data/fars/csv"

def getYears():
  s = []
  for f in os.listdir(CSV_DIR):
    s.append(f)

  return sorted(s)


def processHeader(heads, src):
  for i in range(0, len(heads)):
    head = heads[i].lower()
    heads[i] = {
      "head": head,
      "type": src.get_field(head)
    }


def processSource(src, yr, path):
  with codecs.open(path, "r", encoding="utf-8", errors="ignore") as f:
    context = f.read().split("\n")

    heads = context[0].split(",")
    rows = context[1:]

    processHeader(heads, src)# sources.accident)

    for row in rows:#[0:50]:
      cells = row.split(",")
      if len(cells) == len(heads):
        datum = {}
        for i in range(0, len(cells)):
          h = heads[i]
          if(h["type"]):
            key = h["type"].name
            val = h["type"].convert(cells[i])
            datum[key] = val
        src.data.append(datum)

  #print(len(data["accident"]))

  
def getFiles(years):
  files = {}
  for year in years:
    for f in  os.listdir("{}/{}".format(CSV_DIR, year)):
      if f.lower() not in files:
        files[f.lower()] = 0
      files[f.lower()] += 1
      
  for f in files:
    if files[f] == len(years):
      files[f] = 1
    else:
      files[f] = 0

  return files



    

def getHeaders(years, csv):
  headers = {}
  for year in years:
    path = "{}/{}/{}.csv".format(CSV_DIR, year, csv)
    #with codecs.open(path, "r", encoding="utf-8", errors="replace") as f:
    with open(path, "r") as f:
      context = f.read().split("\n")
      heads = context[0].split(",")
      
      for head in heads:
        if head.lower() not in headers:
          headers[head.lower()] = 0
        headers[head.lower()] += 1

  for head in headers:
    if headers[head] == len(years):
      headers[head] = 1
    else:
      headers[head] = 0

  return headers



if __name__ == "__main__":
  years = getYears()

  files = getFiles(years)
  headers = getHeaders(years, "accident")




'''

  for year in years[-1:]:
    path = "{}/{}".format(CSV_DIR, year)

    for src in sources.get_all():

      processSource(src, year, "{}/{}.csv".format(path, src.name))

    
    print(sources.accident.data[0])
   
'''


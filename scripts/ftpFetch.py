import ftplib
import os
import shutil
import zipfile
#from dbfpy import dbf
#from dbfpy import fields
from datetime import date

FTP_URL = "ftp.nhtsa.dot.gov"
FTP_ROOT = "fars"
FTP = None
DATA_DIR = "../data"
FARS_DIR = "{}/fars".format(DATA_DIR)
ZIP_DIR = "{}/zip".format(FARS_DIR)
CSV_DIR = "{}/csv".format(FARS_DIR)
START = 2001
#END = 2018
if date.today() >= date(date.today().year, 10, 15):
	END = date.today().year - 1
else:
	END = date.today().year - 2

def ftpConnect(url, root):
	ftp = ftplib.FTP(url)
	ftp.login()
	ftp.cwd(root)

	return ftp

def makeDir(path):
	try:
		os.mkdir(path)
	except:
		pass

def fetchZips():
	year = END 
	while year >= START:
		print("fetching zip for {}".format(year))
		ftpDir = "{}/National/".format(year)
		csvFile = "FARS{}NationalCSV.zip".format(year)
		saveTo = "{}/{}.zip".format(ZIP_DIR, year)
		ftpBack = ftpDir.replace(str(year), "..").replace("National", "..")

		FTP.cwd(ftpDir)
			
		FTP.retrbinary("RETR " + csvFile, open(saveTo, "wb").write)

		FTP.cwd(ftpBack)
		os.chmod(saveTo, 0o444)
		year -= 1

def unpackZips():
	year = END
	while year >= START:
		print("unpaking zip for {}".format(year))
		zipDir = "{}/{}.zip".format(ZIP_DIR, year)
		csvDir = "{}/{}".format(CSV_DIR, year)

		print( zipDir)
		
		z = zipfile.ZipFile(zipDir)
		
		makeDir(csvDir)
		print( csvDir)

		for d in z.namelist():
			if d[-3:].lower() == "csv":
				z.extract(d, csvDir)

		for f in os.listdir(csvDir):
			f = "{}/{}".format(csvDir, f)
			os.rename(f, f.lower())

		year -= 1


FTP = ftpConnect(FTP_URL, FTP_ROOT)





if __name__ == "__main__":
	makeDir(DATA_DIR)
	makeDir(FARS_DIR)
	makeDir(ZIP_DIR)
	makeDir(CSV_DIR)
	fetchZips()
	unpackZips()





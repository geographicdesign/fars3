import json

REF_DIR = "../ref_tables"




def readJson(path):
  with open(path, "r") as f:
    return (json.load(f))


def strToFloat(val):
  return(float(val))

class String:
  def __init__(self, name, keys):
    self.name = name
    self.keys = keys
    self.type = "string"

  def convert(self, val):
    if val.strip() == "":
      return None

    return val.strip()

  def is_type(self, key):
    if key.strip() in self.keys:
      return True
    return False


class Float:
  def __init__(self, name, keys):
    self.name = name
    self.keys = keys
    self.type = "float"

  def convert(self, val):
    if val.strip() == "":
      return None

    return(float(val))

  def is_type(self, key):
    if key.strip() in self.keys:
      return True
    return False

class Int:
  def __init__(self, name, keys):
    self.name = name
    self.keys = keys
    self.type = "int"

  def convert(self, val):
    if val.strip() == "":
      return None

    return(int(val))

  def is_type(self, key):
    if key.strip() in self.keys:
      return True
    return False


class Source:
  def __init__(self):
    pass

  def list_fields(self):
    for field in self.fields:
      print ("{} ({})".format(field.name, field.type))
      for key in field.keys:
        print("  {}\n".format(key))


  def get_field(self, key):
    for field in self.fields:
      if field.is_type(key):
        return field
      

  def add_float(self, f_name, f_keys):
    self.fields.append(Float(f_name, f_keys))

  def add_int(self, f_name, f_keys):
    self.fields.append(Int(f_name, f_keys))

  def add_string(self, f_name, f_keys):
    self.fields.append(String(f_name, f_keys))


  def accident_id(self, d):
    i = str(d["st_case"])
    l = len(str(d["state"]))
    i = i[l:]
    return "{}.{}.{}".format(d["year"], d["state"], i)


class Accident(Source):
  def __init__(self, name, fields=[]):
    self.name = name
    self.fields = fields
    self.data = []

    self.add_int("day", ["day"])
    self.add_int("month", ["month"])
    self.add_int("year", ["year"])
    self.add_int("hour", ["hour"])
    self.add_int("minute", ["minute"])

    self.add_int("state", ["state"])
    self.add_int("county", ["county"])
    self.add_int("city", ["city"])
    self.add_int("fatals", ["fatals"])
    self.add_int("st_case", ["st_case"])
    self.add_float("lat", ["latitude"])
    self.add_float("lng", ["longitud"])


  def case_id(self, d):
    return d["st_case"]

  def state(self, d):
    return states[str(d["state"])]["name"]

  def county(self, d):
    if d["county"] == 999:
      return None
    return states[str(d["state"])]["counties"][str(d["county"])]

  def city(self, d):
    if d["city"] == 0 or d["city"] > 9990:
      return None

    try:
      return states[str(d["state"])]["cities"][str(d["city"])]

    except:
      return None

  def coords(self, d):
    if d["lat"] == None or d["lng"] == None:
      return None
    if d["lat"] >= 99.9 and d["lng"] >= 999.9:
      return None
    
    return [ d["lng"], d["lat"] ]


  def datetime(self, d):
    year = d["year"]
    month = "0{}".format(d["month"])[-2:]
    day = "0{}".format(d["day"])[-2:]
    hour = "0{}".format(d["hour"])[-2:]
    minute = "0{}".format(d["minute"])[-2:]

    return "{}-{}-{}T{}:{}:00".format(year, month, day, hour, minute) 



class Vehicle(Source):
  def __init__(self, name, fields=[]):
    self.name = name
    self.fields = fields
    self.data = []

    self.add_int("state", ["state"])
    self.add_int("st_case", ["st_case"])
    self.add_int("veh_no", ["veh_no"])
    self.add_int("num_occ", ["numoccs"])
    self.add_int("make", ["make"])
    self.add_int("model", ["model"])
    self.add_int("body_typ", ["body_typ"])
    self.add_int("mod_year", ["mod_year"])
    self.add_string("vin", ["vin"])
    self.add_int("deaths", ["deaths"])

  def make(self, d):
    try:
      return vehicle_makes[str(d["make"])]
    except:
      return d["make"]


class Person(Source):
  def __init__(self, name, fields=[]):
    self.name = name
    self.fields = fields
    self.data = []

    self.add_int("state", ["state"])
    self.add_int("st_case", ["st_case"])
    self.add_int("veh_no", ["veh_no"])
    self.add_int("age", ["age"])
    self.add_int("sex", ["sex"])
   

  
class Sources:
  def __init__(self):
    self.accident = Accident("accident")
    self.vehicle = Vehicle("vehicle")
    self.person = Person("person")

  def get_all(self) :    
    return [self.accident, self.vehicle, self.person]


 



sources = Sources()

states = readJson("{}/states.json".format(REF_DIR))
vehicle_makes = readJson("{}/vehicle_makes.json".format(REF_DIR))

def main():  
  sources.accident.list_fields()
  sources.vehicle.list_fields()
  






if __name__ == "__main__":
  main() 



